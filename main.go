package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

func auth(ip string) []string {
	resp, err := http.Get("http://" + ip + "/html/home.html")
	if err != nil {
		fmt.Print("Cannot access modem IP!")
		os.Exit(1)
	}
	defer resp.Body.Close()
	var session string
	for _, cookie := range resp.Cookies() {
		if cookie.Name == "SessionID" {
			session = cookie.Raw
		}
	}
	contents, _ := ioutil.ReadAll(resp.Body)
	explode := strings.Split(string(contents), "\n")
	explode = strings.Split(explode[4], " ")
	explode = strings.Split(explode[2], "\"")
	auth := []string{explode[1], session}
	return auth
}

func dataSwitch(op string, ip string, auth []string) {
	url := "http://" + ip + "/api/dialup/mobile-dataswitch"
	var payload *strings.Reader
	if op == "on" {
		payload = strings.NewReader("<response><dataswitch>1</dataswitch></response>")
	} else {
		payload = strings.NewReader("<response><dataswitch>0</dataswitch></response>")
	}
	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("__RequestVerificationToken", auth[0])
	req.Header.Add("Cookie", auth[1])
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
}

func main() {
	start := time.Now()
	dataSwitch("off", os.Args[1], auth(os.Args[1]))
	dataSwitch("on", os.Args[1], auth(os.Args[1]))
	t := time.Now()
	fmt.Printf("Reconnected in %v\n", t.Sub(start))
}
